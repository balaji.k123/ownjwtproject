import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {



  username = 'javainuse'
  password = ''
  invalidLogin = false

  constructor(private router: Router,
    private loginservice: AuthenticationService) { }
errorMsg:string='';
  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password).subscribe(
      data => {
        this.errorMsg="";
        this.router.navigate(['employees'])
        this.invalidLogin = false
      },
      error => {
        console.log(error.error.error);
        if(error.error.error == 'Unauthorized'){
          this.errorMsg="Invalid Credentials"
        }
        
        this.invalidLogin = true

      }
    )
    );
  }
  ngOnInit(): void {
  }

}
