import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username:String ='';
  password:String ='';
  constructor(private router: Router, private registerservice: AuthenticationService) { }

  ngOnInit(): void {
    this.password="";
    this.username="";
  }
  errorMsg:string='';
  checkRegister(){
    (this.registerservice.authenticateRegister(this.username, this.password).subscribe(
      data => {
        this.errorMsg="";
        console.log(Object(data));
        
        this.router.navigate(['login'])
        // this.invalidLogin = false
      },
      error => {
        console.log(error.error.text);
        this.errorMsg = error.error.text;
        
        // this.invalidLogin = true

      }
    )
    );

  }
}
