import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

export class Credentials{
  username!:string;
  password!:string;
}
export class Token{
  token!:string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
loginCred = new Credentials();
tok = new Token();
  constructor(private httpClient:HttpClient) { }
  localBoolean!:boolean;
  authenticate(username:any, password:any) {
    // let localBoolean;
this.loginCred.username=username;
this.loginCred.password=password;


  return this.httpClient.post('http://localhost:8086/login',this.loginCred).pipe(
    map(
      res=>{console.log(res);
this.tok=Object(res);
console.log(this.tok);

        // sessionStorage.setItem('username',username );
        let jwtTok = 'Bearer '+this.tok.token;
        sessionStorage.setItem('token',jwtTok );
        // console.log(typeof(sessionStorage.getItem('token')));
        return res;
      }
      )
  )
   
 
    // if (username === "javainuse" && password === "password") {
    //   sessionStorage.setItem('username', username)
    //   return true;
    // } else {
    //   return false;
    // }
    
  }

  authenticateRegister(username:any, password:any){
    this.loginCred.username=username;
this.loginCred.password=password;
    return this.httpClient.post('http://localhost:8086/register',this.loginCred).pipe(map(res=> {
    console.log(Object(res));
    return res;
  }
    ))
  }

  isUserLoggedIn() {
    // let user = sessionStorage.getItem('username')
    // console.log(!(user === null))
    // return !(user === null)
    let user = sessionStorage.getItem('token')
    // console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
    // sessionStorage.removeItem('username')
    sessionStorage.removeItem('token')
  }
}
